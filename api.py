#!/usr/bin/python
import urllib2
from BeautifulSoup import BeautifulSoup
import re
import ConfigParser


def game_result(domain, game_id):
	"""Getting result of game.

	Keyword arguments:
	domain -- Example: domain is "sibir" for games on http://sibir.en.cx/.
	game_id -- The game's ID.

	"""
	data = urllib2.urlopen(urls.get("mobile", "game-result") % (domain, game_id)).read()
	soup = BeautifulSoup(data)
	teams = soup.findAll("li")

	result = []
	for team in teams:
		result.append(int(re.search("(\d+)", team.a["href"]).group(1)))

	return result

def past_games(domain, game_type=None):
	"""Getting list of past games.

	Keyword arguments:
	domain -- Example: domain is "sibir" for games on http://sibir.en.cx/.
	game_type -- 0 - encounter
				 1 - brainstorming
				 2 - photoextreme
				 3 - wet wars
				 4 - caching
				 5 - photohunt
				 7 - points
				 8 - videohunt
				 9 - quiz
				(default: None)

	"""
	page = 1
	games = None

	result = []
	while page == 1 or games:
		data = urllib2.urlopen(urls.get("mobile", "past-games") % (domain, page)).read()
		soup = BeautifulSoup(data)
		games = soup.findAll("h1", {"class": "gametitle"})

		for game in games:
			if not game_type or re.search(urls.get("standart", "game-type-image") % "(\d+)", game.find("img")['src']).group(1) == game_type:
				result.append(int(re.search(urls.get("mobile", 'game') % (domain, "(\d+)"), game.find("a")['href']).group(1)))

		page += 1

	return result


urls = ConfigParser.RawConfigParser()
urls.read("urls.ini")
